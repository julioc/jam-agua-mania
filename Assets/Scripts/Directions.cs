using UnityEngine;
using System.Collections.Generic;

public enum Direction
{
  Top,
  Right,
  Bottom,
  Left
}

public static class Directions
{
  public const int Count = 4;

  private static readonly Direction[] values = {
    Direction.Bottom,
    Direction.Left,
    Direction.Top,
    Direction.Right
  };

  public static Direction RandomValue
  {
    get {
      return (Direction) Random.Range(0, Count);
    }
  }

  public static Direction[] RandomValues(int count)
  {
    var list = new List<Direction>(values);
    while(count < Count) {
      list.RemoveAt(Random.Range(0, list.Count));
      count += 1;
    }
    return list.ToArray();
  }

  private static readonly IntVector2[] vectors = {
    new IntVector2(0, 1),
    new IntVector2(1, 0),
    new IntVector2(0, -1),
    new IntVector2(-1, 0)
  };

  public static IntVector2 ToIntVector2(this Direction direction)
  {
    return vectors[(int) direction];
  }

  private static readonly Direction[] opposites = {
    Direction.Bottom,
    Direction.Left,
    Direction.Top,
    Direction.Right
  };

  public static Direction GetOpposite(this Direction direction)
  {
    return opposites[(int) direction];
  }

  private static readonly Quaternion[] rotations = {
    Quaternion.identity,
    Quaternion.Euler(0f, 0f, 270f),
    Quaternion.Euler(0f, 0f, 180f),
    Quaternion.Euler(0f, 0f, 90f)
  };

  public static Quaternion ToRotation(this Direction direction)
  {
    return rotations[(int) direction];
  }

  public static Direction GetNextClockwise(this Direction direction)
  {
    return (Direction) (((int) direction + 1) % Count);
  }

  public static Direction GetNextCounterclockwise(this Direction direction)
  {
    return (Direction) (((int) direction + Count - 1) % Count);
  }
}