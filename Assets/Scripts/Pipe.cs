using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
public class Pipe : MonoBehaviour
{
  private SpriteRenderer render;

  private void Awake()
  {
    render = GetComponent<SpriteRenderer>();
  }

  public void UpdateState(List<Direction> connections, bool filled)
  {
    if(connections.Count == 0) {
      render.enabled = false;
    }
    else {
      render.enabled = true;
      render.sprite = Resources.Load<Sprite>(GetSpriteName(connections, filled));
    }
  }

  private string GetSpriteName(List<Direction> connections, bool filled)
  {
    const string spriteNamePrefix = "Sprites/green_";

    string spriteName = spriteNamePrefix;

    if(connections.Contains(Direction.Top)) {
      spriteName += "t";
    }
    if(connections.Contains(Direction.Right)) {
      spriteName += "r";
    }
    if(connections.Contains(Direction.Bottom)) {
      spriteName += "b";
    }
    if(connections.Contains(Direction.Left)) {
      spriteName += "l";
    }

    if(filled) {
      spriteName += "_filled";
    }

    return spriteName;
  }
}