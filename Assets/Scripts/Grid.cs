using UnityEngine;

public class Grid : MonoBehaviour
{
  public int width;
  public int height;
  public GameObject cellPrefab;
  public Vector2 cellSize;

  public float fillSpreadInterval;

  private Cell[,] cells;

  private void Awake()
  {
    GenerateGrid();
  }

  private void OnDrawGizmos()
  {
    var size = new Vector3(width * cellSize.x, height * cellSize.y, 0);
    Gizmos.color = Color.yellow;
    Gizmos.DrawWireCube(transform.position, size);
  }

  private void OnResetGrid()
  {
    DestroyGrid();
    GenerateGrid();
  }

  public Cell GetCell(IntVector2 coords)
  {
    if(coords.x < 0 || coords.x >= width || coords.y < 0 || coords.y >= height) {
      return null;
    }

    return cells[coords.x, coords.y];
  }

  public void StartFilling(float spreadInterval)
  {
    fillSpreadInterval = spreadInterval;
    cells[0,0].Fill(Direction.Left, spreadInterval);
  }

  private void GenerateGrid()
  {
    cells = new Cell[width,height];
    for(int x = 0; x < width; ++x) {
      for(int y = 0; y < height; ++y) {
        var position = new Vector3(
          transform.position.x + (x - (width / 2.0f) + 0.5f) * cellSize.x,
          transform.position.y + (y - (height / 2.0f) + 0.5f) * cellSize.y,
          transform.position.z);
        var cellObject = Instantiate(cellPrefab, position, Quaternion.identity) as GameObject;
        cellObject.transform.parent = transform;

        Cell cell = cellObject.GetComponent<Cell>();
        cell.Init(new IntVector2(x, y));

        cells[x,y] = cell;
      }
    }
  }

  private void DestroyGrid()
  {
    if(cells == null) {
      return;
    }

    foreach(Cell cell in cells) {
      if(cell == null) {
        continue;
      }

      Destroy(cell.gameObject);
    }

    cells = null;
  }
}