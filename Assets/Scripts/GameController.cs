using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
  public LayerMask cellLayerMask;
  public Water water;
  public FakePipe[] fakePipes;
  public FakePipe endPipe;
  public float startTime = 5f;
  public float fillSpreadInterval = 1.5f;
  public float gameOverScreenDelay = 1.5f;
  public float gameRestartDelay = 3f;

  public GameObject backgroundOverlay;
  public GameObject gameOverLostScreen;
  public GameObject gameOverWonScreen;

  private Camera camera;
  private CellHolder holder;
  private Grid grid;

  private bool gameWin;
  private bool gameOver;
  private bool started;
  private float timeCount;

  private Coroutine fillingCoroutine;

  private void Start()
  {
    camera = Camera.main.GetComponent<Camera>();
    holder = GameObject.FindWithTag("CellHolder").GetComponent<CellHolder>();
    grid = GameObject.FindWithTag("Grid").GetComponent<Grid>();

    StartGame();
  }

  private void Update()
  {
    if(!started) {
      timeCount += Time.deltaTime;

      if(timeCount >= startTime) {
        StartFilling();
      }

      water.Move(timeCount / startTime);
    }

    if(!gameOver) {
      if(Input.GetKeyDown(KeyCode.Mouse0)) {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit, 1000.0f, cellLayerMask)) {
        Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
          Cell cell = hit.collider.gameObject.GetComponent<Cell>();
          if(!cell.IsFilled) {
            cell.Connections = holder.PopCell();
          }
        }
      }
    }
  }

  private void StartFilling()
  {
    timeCount = startTime;
    started = true;

    fillingCoroutine = StartCoroutine("FillPipes");
  }

  private IEnumerator FillPipes()
  {
    if(fakePipes != null) {
      foreach(FakePipe pipe in fakePipes) {
        pipe.SetFilled(true);
        yield return new WaitForSeconds(fillSpreadInterval);
      }
    }

    grid.StartFilling(fillSpreadInterval);

    fillingCoroutine = null;
  }

  private void OnResetGrid()
  {
    started = false;
    timeCount = 0f;

    if(fillingCoroutine != null) {
      StopCoroutine(fillingCoroutine);
    }

    if(fakePipes != null) {
      foreach(FakePipe pipe in fakePipes) {
        pipe.SetFilled(false);
      }
      endPipe.SetFilled(false);
    }
  }

  private void OnGameOver(bool win)
  {
    gameWin = win;
    gameOver = true;

    if(win) {
      if(endPipe != null) {
        endPipe.SetFilled(true);
      }
    }

    Invoke("ShowGameOver", gameOverScreenDelay);
  }

  private void ShowGameOver()
  {
    backgroundOverlay.SetActive(true);

    if(gameWin) {
      gameOverWonScreen.SetActive(true);
    }
    else {
      gameOverLostScreen.SetActive(true);
    }

    Invoke("StartGame", gameRestartDelay);
  }

  private void StartGame()
  {
    gameOver = false;
    backgroundOverlay.SetActive(false);
    gameOverWonScreen.SetActive(false);
    gameOverLostScreen.SetActive(false);
    BroadcastMessage("OnResetGrid");
  }
}