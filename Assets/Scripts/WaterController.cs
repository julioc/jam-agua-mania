using UnityEngine;
using System.Collections.Generic;

public class WaterController : MonoBehaviour
{
  public GameObject leakPrefab;

  private Grid grid;
  private List<GameObject> leaks = new List<GameObject>();

  private void Awake()
  {
    grid = GetComponent<Grid>();
  }

  private void OnSpread(Cell origin)
  {
    bool leak = false;
    bool win = false;
    foreach(Direction direction in origin.Connections) {
      if(origin.FillOrigin == direction) {
        continue;
      }

      IntVector2 coords = origin.Coordinates + direction.ToIntVector2();
      Cell cell = grid.GetCell(coords);
      if(coords.x == 7 && coords.y == 5) {
        win = true;
      }
      else {
        if(cell == null) {
          IntVector2 delta = direction.ToIntVector2();
          Vector3 position = origin.transform.position + new Vector3(delta.x, delta.y, 0);
          object[] leakInfo = {position, direction};
          SendMessageUpwards("OnLeak", leakInfo);
        }
        else if(!cell.Fill(direction.GetOpposite(), grid.fillSpreadInterval)) {
          leak = true;
        }
      }
    }

    if(win && !leak) {
      SendMessageUpwards("OnGameOver", true);
    }
  }

  private void OnLeak(object[] leakInfo)
  {
    ShowLeak((Vector3) leakInfo[0], (Direction) leakInfo[1]);
    SendMessageUpwards("OnGameOver", false);
  }

  private void OnResetGrid()
  {
    foreach(GameObject leak in leaks) {
      Destroy(leak);
    }

    leaks.Clear();
  }

  private void ShowLeak(Vector3 position, Direction direction)
  {
    Quaternion rotation = direction.ToRotation();
    var leakObject = Instantiate(leakPrefab, position, rotation) as GameObject;
    leakObject.transform.parent = transform;
    leaks.Add(leakObject);
  }
}