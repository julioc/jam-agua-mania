using UnityEngine;

public class FakePipe : MonoBehaviour
{
  public Sprite filledSprite;

  private SpriteRenderer render;
  private Sprite defaultSprite;

  private void Awake()
  {
    render = GetComponent<SpriteRenderer>();
    defaultSprite = render.sprite;
  }

  public void SetFilled(bool filled)
  {
    render.sprite = filled ? filledSprite : defaultSprite;
  }
}