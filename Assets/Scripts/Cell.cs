using UnityEngine;
using System.Collections.Generic;

public class Cell : MonoBehaviour
{
  public GameObject pipePrefab;

  private IntVector2 _coordinates;

  private List<Direction> _connections = new List<Direction>();
  private Pipe pipe;

  private bool filled;
  private Direction _fillOrigin;

  public IntVector2 Coordinates
  {
    get {
      return _coordinates;
    }
  }

  public List<Direction> Connections
  {
    get {
      return _connections;
    }
    set {
      _connections = value;
      if(pipe != null) {
        pipe.UpdateState(_connections, filled);
      }
    }
  }

  public Direction FillOrigin
  {
    get {
      return _fillOrigin;
    }
    private set {
      _fillOrigin = value;
    }
  }

  public bool IsFilled
  {
    get {
      return filled;
    }
  }

  public void Init(IntVector2 coords)
  {
    _coordinates = coords;
  }

  public bool Fill(Direction origin, float spreadInterval)
  {
    if(_connections.Count == 0 || !_connections.Contains(origin)) {
      object[] leakInfo = {transform.position, origin.GetOpposite()};
      SendMessageUpwards("OnLeak", leakInfo);
      return false;
    }

    if(filled) {
      return true;
    }

    FillOrigin = origin;

    filled = true;
    pipe.UpdateState(_connections, filled);
    Invoke("FillNeighbours", spreadInterval);

    return true;
  }

  private void Awake()
  {
    CreatePipe();
  }

  private void CreatePipe()
  {
    var pipeObject = Instantiate(pipePrefab, transform.position, Quaternion.identity) as GameObject;
    pipeObject.transform.parent = transform;

    pipe = pipeObject.GetComponent<Pipe>();
    pipe.UpdateState(_connections, filled);
  }

  public void FillNeighbours()
  {
    SendMessageUpwards("OnSpread", this);
  }
}