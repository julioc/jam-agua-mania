using UnityEngine;

public class Water : MonoBehaviour
{
  public Vector3 delta;

  private Vector3 starting;
  private Vector3 target;

  private void Awake()
  {
    starting = transform.position;
    target = starting + delta;
  }

  public void Move(float p)
  {
    transform.position = Vector3.Lerp(starting, target, p);
  }
}