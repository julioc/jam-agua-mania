using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CellHolder : MonoBehaviour
{
  public GameObject cellPrefab;
  [RangeAttribute(0.0f,1.0f)]
  public float threeConnectionsChance;
  [RangeAttribute(0.0f,1.0f)]
  public float fourConnectionsChance;
  public Transform spawnPoint;

  public int amount = 4;
  public float spawnInterval = 1.2f;

  private Queue<Cell> cells = new Queue<Cell>();
  private Coroutine generateCoroutine;

  private void Awake()
  {
    ResetCells();
  }

  private void OnResetGrid()
  {
    ResetCells();
  }

  public List<Direction> PopCell()
  {
    Cell cell = cells.Dequeue();
    if(cell == null) {
      return null;
    }

    List<Direction> connections = cell.Connections;

    GenerateCell(cell);

    return connections;
  }

  public void ResetCells()
  {
    if(cells.Count > 0) {
      foreach(Cell cell in cells) {
        Destroy(cell.gameObject);
      }
      cells.Clear();
    }

    if(generateCoroutine != null) {
      StopCoroutine(generateCoroutine);
    }
    generateCoroutine = StartCoroutine(GenerateCells());
  }

  private IEnumerator GenerateCells()
  {
    while(cells.Count < amount) {
      GenerateCell();
      yield return new WaitForSeconds(spawnInterval);
    }

    generateCoroutine = null;
  }

  private void GenerateCell(Cell cell=null)
  {
    if(cell == null) {
      GameObject obj = Instantiate(cellPrefab);
      obj.transform.parent = transform;
      cell = obj.GetComponent<Cell>();
    }

    cell.Connections = GenerateCellConnections();
    cell.transform.position = spawnPoint.position;

    cells.Enqueue(cell);
  }

  private List<Direction> GenerateCellConnections()
  {
    int count = 2;
    if(Random.Range(0.0f, 1.0f) <= threeConnectionsChance) {
      count = 3;
    }
    if(Random.Range(0.0f, 1.0f) <= fourConnectionsChance) {
      count = 4;
    }

    return new List<Direction>(Directions.RandomValues(count));
  }
}