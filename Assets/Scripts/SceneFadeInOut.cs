using UnityEngine;
using UnityEngine.UI;

public class SceneFadeInOut : MonoBehaviour
{
  public float fadeSpeed = 0.5f;
  public string nextScene;
  public float timeToChange = 1.0f;

  private bool sceneStarting = true;
  private float elapsedTime;

  private Image image;

  void Awake ()
  {
    image = GetComponent<Image>();
  }


  void Update ()
  {
    if(sceneStarting) {
      StartScene();
    }
    else if(timeToChange > 0) {
      elapsedTime += Time.deltaTime;

      if(elapsedTime > timeToChange) {
        EndScene();
      }
    }
  }


  void FadeToClear ()
  {
    image.color = Color.Lerp(image.color, Color.clear, fadeSpeed * Time.deltaTime);
  }


  void FadeToBlack ()
  {
    image.color = Color.Lerp(image.color, Color.black, fadeSpeed * Time.deltaTime);
  }


  void StartScene ()
  {
    FadeToClear();

    if(image.color.a <= 0.05f)
    {
      image.color = Color.clear;
      image.enabled = false;

      sceneStarting = false;
    }
  }


  public void EndScene ()
  {
    // Make sure the texture is enabled.
    image.enabled = true;

    // Start fading towards black.
    FadeToBlack();

    // If the screen is almost black...
    if(image.color.a >= 0.95f)
      // ... reload the level.
      Application.LoadLevel(nextScene);
  }
}